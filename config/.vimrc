" 设置<leader>键
let mapleader=';'

" 关闭兼容模式
set nocompatible

" 帮助文档显示为中文
set helplang=cn
" 总是使用英文菜单
set langmenu=none
" VIM内部字符编码
set encoding=utf-8
" 终端字符编码
set termencoding=utf-8
" 新建文件编码
set fileencoding=utf-8
" 打开文件支持编码
set fileencodings=utf-8
set fileencodings+=ucs-bom,cp936,gb18030,big5,euc-jp,euc-kr,gbk,latin-1
" 脚本编码
scriptencoding utf-8

" 行结束符格式
set fileformat=unix
" 可支持行结束符格式
set fileformats=unix

" 设置 runtimepath 来启用 vim-plug 插件
exec 'set rtp+=' . fnameescape($HOME . '/.vim/plugs/vim-plug/')
call plug#begin($HOME . '/.vim/plugs')

" vim-color-solarized 配色方案插件
Plug 'altercation/vim-colors-solarized'

" indentline 缩进线插件
" Plug 'yggdroot/indentline'
" let g:indentLine_char = ''
" let g:indentLine_first_char = ''
" let g:indentLine_char_list = ['⎸', '¦', '┆', '┊']
" let g:indentLine_leadingSpaceChar = '˰'
" let g:indentLine_leadingSpaceEnabled = 1

" whitespace 检测有效字符行未空格插件
Plug 'ntpeters/vim-better-whitespace'

" undotree 撤销树插件
Plug 'mbbill/undotree'
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_WindowLayout = 4

" nerdtree nerdtree 文件浏览器插件
Plug 'scrooloose/nerdtree'
" let g:NERDTreeIgnore = [ 'target$[[dir]]', 'META-INF$[[dir]]', '\~$' ]
" let g:NERDTreeShowBookmarks = 1
" let g:NERDTreeShowLineNumbers = 1
" let g:NERDTreeMinimalUI = 1
" let g:NERDTreeCascadeOpenSingleChildDir = 0
" let g:NERDTreeDirArrowExpandable="+"
" let g:NERDTreeDirArrowCollapsible="~"
let g:NERDTreeAutoDeleteBuffer = 1
let g:NERDTreeMinimalMenu = 1
let g:NERDTreeShowHidden = 1
let g:NERDTreeWinSize = 30
let g:NERDTreeWinSizeMax = 50
let g:NERDTreeMapToggleZoom = '<Space>'

" nerdcommenter 注释插件
Plug 'scrooloose/nerdcommenter'
let g:NERDSpaceDelims = 1
let g:NERDRemoveExtraSpaces = 1
let g:NERDTrimTrailingWhitespace = 1

" ack 跨文件关键字搜索插件
Plug 'mileszs/ack.vim'
let g:ack_qhandler = "copen 20"
let g:ack_apply_qmappings = 1
let g:ackpreview = 1
" let g:ackhighlight = 1

" ctrlp 插件
Plug 'kien/ctrlp.vim'
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_working_path_mode = ''
let g:ctrlp_match_window = 'bottom,order:ttb,min:1,max:10,results:10'
let g:ctrlp_follow_symlinks = 2
let g:ctrlp_custom_ignore = {
  \ 'dir': '\v[\/]\.(git|hg|svn|rvm)$',
  \ 'file': '\v[\/]\.(exe|so|dll|zip|tar|gz|pyc|class)$'
\ }
let g:ctrlp_max_files = 0
let g:ctrlp_mruf_max = 100
" ctrlp 窗口快捷键功能
" <F5> 清除缓存,刷新当前模式下模糊搜索文件列表
" <c-f> <c-b> 在 file,buffer,mru 三种模式间来回切换
" <c-d> 切换文件搜索方式为文件名搜索或全路径搜索
" <c-r> 切换文件搜索方式为正则模式或非正则模式
" <c-j> <c-k> ctrlp 窗口中上下移动并选中光标行
" <c-t> <c-v> <c-x> 预览选中行,并在一个新窗口中打开buffer
" <c-n> <c-p> 跳转上一个/下一个模糊搜索历史,非常有用
" <c-y> 创建新文件和它的父级目录,打开一个新的buffer,:w之后才会存盘写入文件
" <c-z> 标记多个文件 <c-o> 同时打开已标记的多个文件

" vim-surround 插件
Plug 'tpope/vim-surround'

" vim-easymotion 插件
Plug 'Lokaltog/vim-easymotion'
let g:EasyMotion_startofline = 0
let g:EasyMotion_do_mapping = 0

" vim-easy-align 插件
Plug 'junegunn/vim-easy-align'

" emmet-vim 插件
Plug 'mattn/emmet-vim'
let g:user_emmet_install_global = 0

" AnsiEsc 解析终端转义字符插件
Plug 'vim-scripts/AnsiEsc.vim'

" vim-vue vue 文件语法高亮及缩进
Plug 'posva/vim-vue'

" vim-pug pug 文件语法高亮及缩进
Plug 'digitaltoad/vim-pug'

" vim-stylus stylus 文件语法高亮及缩进
Plug 'wavded/vim-stylus'

" vim-markdown markdown 文件语法高亮及缩进
" Plug 'plasticboy/vim-markdown'

" 自动格式化插件
Plug 'Chiel92/vim-autoformat'
let g:formatdef_sqlformat = '"sqlformat -k upper -r -s -"'
let g:formatters_sql = ['sqlformat']

" exvim 配置文件解析插件
Plug 'exvim/ex-config'

" ex-utility 插件
Plug 'exvim/ex-utility'

" ex-vimentry 创建和设置一个项目插件
Plug 'exvim/ex-vimentry'

" ex-project 文件浏览器插件
Plug 'exvim/ex-project'

" ex-gsearch 全局搜索插件
" Plug 'exvim/ex-gsearch'

" ex-tagselect 查找符号插件
Plug 'exvim/ex-tags'

" ex-symbol 查找符号 插件
" Plug 'exvim/ex-symbol'

" ex-cscope 查找符号插件
Plug 'exvim/ex-cscope'

" ex-qfix 插件
Plug 'exvim/ex-qfix'

" ex-hierarchy 导出类继承图插件
" Plug 'exvim/ex-hierarchy'

" ex-tagbar 类标签插件
Plug 'exvim/ex-tagbar'
let g:tagbar_sort = 0
let g:tagbar_map_preview = '<CR>'
let g:tagbar_map_close = '<leader><Esc>'
let g:tagbar_map_zoomwin = '<Space>'
let g:tagbar_width = 30
let g:tagbar_zoomwidth = 50
let g:tagbar_autofocus = 1
let g:tagbar_iconchars = ['+', '-']

" use command ':TagbarGetTypeConfig lang' change your settings
let g:tagbar_type_javascript = {
  \ 'ctagsbin': 'ctags',
  \ 'kinds' : [
    \ 'v:global variables:0:0',
    \ 'c:classes',
    \ 'p:properties:0:0',
    \ 'm:methods',
    \ 'f:functions',
    \ 'r:object',
  \ ],
\ }
let g:tagbar_type_c = {
  \ 'kinds' : [
    \ 'd:macros:0:0',
    \ 'p:prototypes:0:0',
    \ 'g:enums',
    \ 'e:enumerators:0:0',
    \ 't:typedefs:0:0',
    \ 's:structs',
    \ 'u:unions',
    \ 'm:members:0:0',
    \ 'v:variables:0:0',
    \ 'f:functions',
  \ ],
\ }
let g:tagbar_type_cpp = {
  \ 'kinds' : [
    \ 'd:macros:0:0',
    \ 'p:prototypes:0:0',
    \ 'g:enums',
    \ 'e:enumerators:0:0',
    \ 't:typedefs:0:0',
    \ 'n:namespaces',
    \ 'c:classes',
    \ 's:structs',
    \ 'u:unions',
    \ 'f:functions',
    \ 'm:members:0:0',
    \ 'v:variables:0:0',
  \ ],
\ }

" ex-matchit 增强%跳转插件
Plug 'exvim/ex-matchit'
" let b:match_words = '\<if\>:\<elif\>:\<else\ if\>:\<else\>:\<endif\>'
" let b:matchit_ignorecase = 1

" ex-java java导包插件
Plug 'exvim/ex-java'
let g:ex_java_search_path = '~/.__source'
let g:ex_mvn_cache_path = '~/.mvnCache'
let g:sortedPackage = ["java", "javax", "org", "com", "lombok", "io"]
let g:packageSepDepth = 1

" ex-sql vim-mysql插件
Plug 'exvim/ex-sql'
let g:kyo_sql_host = 'localhost'
let g:kyo_sql_user = 'root'
let g:kyo_sql_pwd = '123456'
let g:kyo_sql_port = 3306
let g:kyo_sql_db = 'mysql'
let g:kyo_remote_host = ''
let g:kyo_remote_port = ''
let g:kyo_ssh_conn_name = ''

" ex-mog vim-mongodb插件
Plug 'exvim/ex-mog'
let g:littlelot_mog_host = 'localhost'
let g:littlelot_mog_port = 27017
let g:littlelot_mog_db = 'test'

" vim-fugitive 插件
Plug 'tpope/vim-fugitive'

" vim-plug 插件启用完毕
call plug#end()

" 识别文件类型插件
" filetype plugin indent on
" 开启语法高亮
" syntax on

" 设置VIM显示背景
if has('gui_running')
  set background=dark
else
  set background=dark
  " 设置终端打开VIM 显示颜色数目为256
  set t_Co=256
  let g:solarized_termcolors = 256
endif
" 配色方案
colorscheme solarized

" 开启备份
set backup
let data_dir = $HOME . '/.data/'
let backup_dir = data_dir . 'backup'
let swap_dir = data_dir . 'swap'
if finddir(data_dir) == ''
  silent call mkdir(data_dir)
endif
if finddir(backup_dir) == ''
  silent call mkdir(backup_dir)
endif
if finddir(swap_dir) == ''
  silent call mkdir(swap_dir)
endif
unlet backup_dir
unlet swap_dir
unlet data_dir

" 设置备份文件位置
set backupdir=$HOME/.data/backup
" 设置交换文件位置
set directory=$HOME/.data/swap

" 命令历史保存数
set history=50
" 交换文件写入磁盘间隔
set updatetime=1000
" 自动重新读入
set autoread
" 用户搜索的最大内存量
set maxmempattern=1000
" 显示行号
set nu
" 自动换行
set wrap
" 光标上下两侧屏幕行数
set scrolloff=2
" 命令行补全模式
set wildmenu
" 关闭最后一行显示命令功能
set noshowcmd
" 显示光标行号和列号
set ruler
" 允许改变不保存缓冲区
set hidden
" 去掉欢迎界面,任意按键提示
set shortmess=aoOtTI
" 执行宏不重画屏幕
set lazyredraw
" 最后一行文字换行
set display+=lastline
" 命令使用屏幕行数
set cmdheight=1
" 显示状态行
set laststatus=2
" 设置命令行和状态栏
set statusline=%F%m%r\ ASCII=\%b,HEX=\%B,%l,%c%V\ %L-%p%%
" 设置窗口的标题
set titlestring=%t\ (%{expand(\"%:p:.:h\")}/)

" 不改变当前工作目录的值
if v:version >= 703
  set noacd
endif
" 插入模式补全函数时显示函数参数
set showfulltag
" 设置80高亮
" set cc=80
" 在插入模式下允许退格所有字符
set backspace=indent,eol,start
" 自动缩进 autoinent
set ai
" 开启新行时智能自动缩进 适用于C语言 smartindent
set si
" 插入模式下将tab转化成空格
set expandtab
" 设置<>或=缩进命令一次缩进为多少个空格
set shiftwidth=4
" 设置插入文本的最大宽度
" set textwidth=80
" 设置tab键对应多少个空格
set tabstop=4
" 设置退格键可以删除一个tab对应的空格数
set softtabstop=4
" 指定退格键可以删除一个shiftwidth宽度的空格
set smarttab
" 应用在块可视模式，光标可以定位在不是真实字符上
set ve=block
" 按=自动缩进的选项
set cinoptions=>s,e0,n0,f0,{0,}0,^0,:0,=s,l0,b0,g0,hs,ps,ts
set cinoptions+=is,+s,c3,C0,0,(0,us,U0,w0,W0,m0,j0,)20,*30

" 官方diff设置
set diffexpr=g:MyDiff()
function! g:MyDiff()
  let opt = '-a --binary -w '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  silent execute '!' .  'diff ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
endfunction

" 设置ctrl+a和ctrl+x不递增或递减
set nf=

" foldmethod 设置当前窗口使用的折叠方式
set foldmethod=indent
" 设置折叠开始和结束标志
set foldmarker={,}
" 设置折叠级别 高于此级别的折叠会被关闭
set foldlevel=9999
" 设置diff模式选项
set diffopt=filler,context:9999

" 插入括号时,此屏幕能看到匹配的括号才会跳转
set showmatch
" 显示配对括号的时间 时间单位为十分之一秒数
set matchtime=0

" 搜索时会实时匹配并跳转显示,但是按ESC或没有匹配到会回到原来位置
set incsearch
" 高亮搜索关键词
set hlsearch
" 忽略大小写搜索
set ignorecase
" 如果关键词包含大写不使用ignorecase选项
set smartcase

" 使得注释换行时自动加上前导的空格和星号
" set formatoptions=tcqro
" 设定在任何模式下鼠标都可用
set mouse=
" 在单词中间断行
" set nolinebreak

" 设置grep命令使用的程序
set grepprg=lid\ -Rgrep\ -s
" 识别grep命令输出格式
set grepformat=%f:%l:%m

" 设置 HTML 缩进长度
" function! LittlelotHtmlSet(...) {{{
function! LittlelotHtmlSet(...)
  setlocal shiftwidth=2
  setlocal tabstop=2
  setlocal softtabstop=2
  if a:0 && a:1 ==# 1
    EmmetInstall
  endif
endfunction
" }}}

" 恢复上一次编辑该文件时光标位置
" function! LittlelotRecoverCursor() {{{
function! LittlelotRecoverCursor()
  if line("'\"") > 0 && line("'\"") <= line("$")
    exe "normal g`\""
  endif
endfunction
" }}}

" 自动打开关闭或定位文件浏览窗口,nerdtree和ex之间切换
" function! NERDTreeAndEXProjectToggle() {{{
function! NERDTreeAndEXProjectToggle(...)
  if a:0 && a:1 ==# 0
    if exists ( ':NERDTreeToggle' ) && exists ( ':EXProjectToggle' ) && exists ( 'g:NERDTreeAndEXProjectToggle' )
      if g:NERDTreeAndEXProjectToggle ==# 0
        exec 'NERDTreeToggle'
      elseif g:NERDTreeAndEXProjectToggle ==# 1
        exec 'EXProjectToggle'
      endif
    elseif exists ( ':NERDTreeToggle' )
      exec 'NERDTreeToggle'
    endif
  elseif a:0 && a:1 ==# 1
    if exists ( ':NERDTreeFind' ) && exists ( ':EXProjectFind' ) && exists ( 'g:NERDTreeAndEXProjectToggle' )
      if g:NERDTreeAndEXProjectToggle ==# 0
        exec 'NERDTreeFind'
      elseif g:NERDTreeAndEXProjectToggle ==# 1
        exec 'EXProjectFind'
      endif
    elseif exists ( ':NERDTreeFind' )
      exec 'NERDTreeFind'
    endif
  endif
endfunction
" }}}

" 复制文本到剪贴板
" function! CopyToClipboard() {{{
function! CopyToClipboard() range
  let lines = getline(a:firstline, a:lastline)
  let col1 = getpos("'<")[2]
  let col2 = getpos("'>")[2]
  let lines[-1] = lines[-1][:col2 - (&selection == 'inclusive' ? 1 : 2)]
  let lines[0] = lines[0][col1 - 1:]
  call system('xclip -selection clipboard', join(lines, "\n"))
  echom a:lastline - a:firstline + 1 . " 行复制了"
endfunction
" }}}

" 从剪贴板粘贴文本
" function! PasteFromClipboard() {{{
" function! PasteFromClipboard()
  " let lines = system('xclip -selection clipboard -o')
  " if lines == ""
    " return
  " endif
  " call append(line("."), substitute(lines, "\%x00", "\r", "g"))
  " echom "多了 " . len(split(lines, "\n")) . " 行"
" endfunction
" }}}

" 修复vue文件中js和css代码使用NERDCommenter插件无法注释的问题
let g:ft = ''
" function! NERDCommenter_before() {{{
function! NERDCommenter_before()
  if &ft == 'vue'
    let g:ft = 'vue'
    let stack = synstack(line('.'), col('.'))
    if len(stack) > 0
      let syn = synIDattr((stack)[0], 'name')
      if len(syn) > 0
        exe 'setf ' . substitute(tolower(syn), '^vue_', '', '')
      endif
    endif
  endif
endfunction
" }}}

" function! NERDCommenter_after() {{{
function! NERDCommenter_after()
  if g:ft == 'vue'
    setf vue
    let g:ft = ''
  endif
endfunction
" }}}

" function! LittlelotGlobalSearch(...) range {{{
function! LittlelotGlobalSearch(...) range
  if a:0 == 0
    let lines = getline(a:firstline, a:lastline)
    let col1 = getpos("'<")[2]
    let col2 = getpos("'>")[2]
    let lines[-1] = lines[-1][:col2 - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][col1 - 1:]
    let keyword = escape(substitute(join(lines), '\s', '', 'g'), '/.^$*[]\')
  else
    let keyword = a:1
  endif

  let ackcmd = "Ack!"
  if vimentry#check('folder_filter_mode', 'exclude')
    let folderfilters = vimentry#get('folder_filter', [])
    if !empty(folderfilters)
      for folderfilter in folderfilters
        let folderfilter = substitute(folderfilter, '\^\|\$', '', 'g')
        let ackcmd .= ' --ignore-dir=' . folderfilter
      endfor
    endif
  endif

  " let ackcmd .= ' --type=java' . ' --type=xml' . ' --type=yaml' . ' --type=js' . ' --type=css' . ' --type=html'
  let ackcmd .= " '" . keyword . "'"
  exec ackcmd
endfunction
" }}}

" 不使用Ex mode,将Q格式化,不会进入Ex mode
map Q gq
" 关闭搜索高亮
nnoremap <leader>nh :noh<CR>
nnoremap <leader>wp :set wrap!<CR>
" 开关粘贴模式(保留粘贴原文格式)
nnoremap <leader>pa :set paste!<CR>
" 开关行号显示
nnoremap <leader>nu :set number!<CR>
" 用空格键来开关折叠
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>

" 设置分屏窗口之间切换
nnoremap <C-H> <C-W><Left>
nnoremap <C-J> <C-W><Up>
nnoremap <C-K> <C-W><Down>
nnoremap <C-L> <C-W><Right>

" <C-K> 插入模式下光标向上移动
" inoremap <C-K> <Up>
" <C-J> 插入模式下光标向下移动
" inoremap <C-J> <Down>
" <C-H> 插入模式下光标向左移动
" inoremap <C-H> <Left>
" <C-L> 插入模式下光标向右移动
" inoremap <C-L> <Right>

" normal模式下清除行尾 ^M 符号
nnoremap <leader>M :%s/\r$//g<CR>:noh<CR>

" 文件之间复制粘贴
xnoremap "y :w! /tmp/tmp.vim<CR>
nnoremap "y :w! /tmp/tmp.vim<CR>
nnoremap "p :r  /tmp/tmp.vim<CR>

" clipboard 特性,系统剪贴板复制粘贴
if has('clipboard')
  xnoremap <leader>y "+y
  nnoremap <leader>p "+p
endif

if executable('xclip')
  xnoremap <leader>y :call CopyToClipboard()<CR>
  nnoremap <leader>y :call CopyToClipboard()<CR>
endif

" 编辑 .vimrc 文件
nnoremap <leader>ev :vs $MYVIMRC<CR>
" 重载 .vimrc 文件
nnoremap <leader>sv :source $MYVIMRC<CR>
" 当前缓冲区文件列表
nnoremap <unique> <leader>B :CtrlPBuffer<CR>
" 最近打开文件列表历史
nnoremap <unique> <leader>F :CtrlPMRUFiles<CR>
" logger 打印语句映射
nnoremap <leader>sl :exec "normal yssflogger.info\r"<CR>
" java 打印语句映射
nnoremap <leader>sp :exec "normal yssfSystem.out.println\r"<CR>
" 定位文件所在目录树位置
nnoremap <leader>sc :exec "call NERDTreeAndEXProjectToggle(1)"<CR>

" 重定义 ';' 默认映射
nnoremap \ ;
xnoremap \ ;

" easymotion 快速移动键映射
" 单窗口
map <leader>f <Plug>(easymotion-s)
map <leader>h <Plug>(easymotion-bd-W)
map <leader>l <Plug>(easymotion-bd-E)
map <leader><leader>h <Plug>(easymotion-bd-w)
map <leader><leader>j <Plug>(easymotion-lineanywhere)
map <leader><leader>k <Plug>(easymotion-bd-jk)
map <leader><leader>l <Plug>(easymotion-bd-e)
" 跨窗口
nmap <leader>f <Plug>(easymotion-overwin-f)
nmap <leader><leader>h <Plug>(easymotion-overwin-w)

" easy-align 快速对齐映射
xmap <space> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" exvim 打开QuickFix窗口映射
nnoremap <unique> <silent> <leader>V :EXQFixToggle<CR>
" exvim 打开JSearch窗口映射
nnoremap <unique> <silent> <leader>J :EXJavaToggle<CR>
" exvim 打开MySQL窗口映射
nnoremap <unique> <silent> <leader>N :EXSqlToggle<CR>
" exvim 打开MySQL预览窗口映射
nnoremap <unique> <silent> <leader>P :EXSqlPreviewToggle<CR>
" exvim 打开MongoDB窗口映射
nnoremap <unique> <silent> <leader>K :EXMogToggle<CR>
" undotree 打开撤销树窗口映射
nnoremap <unique> <silent> <leader>U :UndotreeToggle<CR>
" exvim 打开标签栏窗口映射
nnoremap <unique> <silent> <leader>L :TagbarToggle<CR>
" 打开文件浏览窗口映射
nnoremap <unique> <silent> <leader>E :exec "call NERDTreeAndEXProjectToggle(0)"<CR>
" vim-fugitive 打开GitBlame窗口映射
nnoremap <unique> <silent> <leader>G :G blame<CR>
" ack快速全文搜索
nnoremap <unique> <silent> <leader>S :call LittlelotGlobalSearch(expand('<cword>'))<CR>
xnoremap <unique> <silent> <leader>S :call LittlelotGlobalSearch()<CR>
nnoremap <unique> <silent> <leader>C :cclose<CR>

command! -bang -nargs=* -complete=file LGS call LittlelotGlobalSearch(<q-args>)

" 自动格式化映射
nnoremap <F2> :Autoformat<CR>
xnoremap <F2> :Autoformat<CR>

" HTML CSS文件的缩进
augroup littlelot
  autocmd!

  " 保存文件时自动去除行尾空格
  autocmd BufWrite * StripWhitespace
  autocmd BufEnter * :syntax sync fromstart
  autocmd FileType text setlocal textwidth=78
  " 设置ctags查找tags文件到当前目录下查找
  autocmd BufNewFile,BufEnter * set cpoptions+=d

  " for avs syntax file. 对于avs文件设置avs语法
  autocmd BufNewFile,BufRead *.avs set syntax=avs
  autocmd BufNewFile,BufRead *.txt setlocal ft=markdown
  " 打开文件时恢复上次编辑光标位置,最后一次保存时光标所在位置
  autocmd BufReadPost * exec "call LittlelotRecoverCursor()"
  autocmd BufWinEnter *.sql exec "call exsql#KyoMySQLAutoGen()"
  autocmd BufNewFile,BufRead *.vim exec "call LittlelotHtmlSet()"
  autocmd BufNewFile,BufRead *.yml exec "call LittlelotHtmlSet()"
  autocmd BufNewFile,BufRead *.yaml exec "call LittlelotHtmlSet()"
  autocmd BufNewFile,BufRead *.sql exec "call LittlelotHtmlSet()"
  autocmd BufNewFile,BufRead *.sql exec "call exsql#KyoMySQLAutoGen()"
  autocmd BufNewFile,BufRead *.java exec "call exjava#LittlelotAutoGen()"
  autocmd BufNewFile,BufRead *.js exec "call exmog#LittlelotMongoDBAutoGen()"
  autocmd BufNewFile,FileType vim,sql exec "call LittlelotHtmlSet()"
  autocmd BufNewFile,FileType xml,html,css,vue,javascript,json,groovy exec "call LittlelotHtmlSet(1)"
augroup end
