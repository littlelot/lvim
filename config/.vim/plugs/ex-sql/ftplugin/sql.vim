" mappings and iabbrs

if maparg('<leader>sq', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>sq :call exsql#KyoMySQLCmdView(0)<CR>
endif

if maparg('<leader>sq', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>sq :call exsql#KyoMySQLCmdView(1)<CR>
endif

if maparg('<leader>sg', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>sg :call exsql#KyoMySQLGenConfig()<CR>
endif

if maparg('<leader>st', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>st :call exsql#KyoMySQLGenTime()<CR>
endif

if maparg('<leader>sm', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>sm :call exsql#GenerateTableCrudSql('select-count', expand('<cword>'), 0)<CR>
endif

if maparg('<leader>sm', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>sm :call exsql#GenerateTableCrudSql('select-count', '', 1)<CR>
endif

if maparg('<leader>sn', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>sn :call exsql#GenerateTableCrudSql('select-all', expand('<cword>'), 0)<CR>
endif

if maparg('<leader>sn', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>sn :call exsql#GenerateTableCrudSql('select-all', '', 1)<CR>
endif

if maparg('<leader>si', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>si :call exsql#GenerateTableCrudSql('insert', expand('<cword>'), 0)<CR>
endif

if maparg('<leader>si', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>si :call exsql#GenerateTableCrudSql('insert', '', 1)<CR>
endif

if maparg('<leader>su', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>su :call exsql#GenerateTableCrudSql('update', expand('<cword>'), 0)<CR>
endif

if maparg('<leader>su', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>su :call exsql#GenerateTableCrudSql('update', '', 1)<CR>
endif

if maparg('<F5>', 'i') == ""
    inoremap <silent> <unique> <buffer> <F5> <C-R>=exsql#TriggerComplete('db')<CR>
endif

if maparg('<F3>', 'i') == ""
    inoremap <silent> <unique> <buffer> <F3> <C-R>=exsql#TriggerComplete('table')<CR>
endif

if maparg('<F2>', 'i') == ""
    inoremap <silent> <unique> <buffer> <F2> <C-R>=exsql#TriggerComplete('func')<CR>
endif

if maparg('S?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> S? SELECT FROM WHERE
endif

if maparg('U?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> U? UPDATE FROM WHERE
endif

if maparg('D?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> D? DELETE FROM WHERE
endif

if maparg('I?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> I? INSERT INTO () VALUES ()
endif

if maparg('kyomysql?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> kyomysql? <C-R>=exsql#KyoMySQLGenConfig()<CR>
endif

if maparg('kyotime?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> kyotime? <C-R>=exsql#KyoMySQLGenTime()<CR>
endif

if maparg('select?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> select? <C-R>=exsql#KyoSQLAbbr('select')<CR>
endif

if maparg('create?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> create? <C-R>=exsql#KyoSQLAbbr('create')<CR>
endif

if maparg('func?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> func? <C-R>=exsql#KyoSQLAbbr('func')<CR>
endif

if maparg('proc?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> proc? <C-R>=exsql#KyoSQLAbbr('procedure')<CR>
endif

if maparg('declare?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> declare? <C-R>=exsql#KyoSQLAbbr('sql_declare')<CR>
endif

if maparg('if?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> if? <C-R>=exsql#KyoSQLAbbr('sql_if')<CR>
endif

if maparg('case?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> case? <C-R>=exsql#KyoSQLAbbr('sql_case')<CR>
endif

if maparg('while?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> while? <C-R>=exsql#KyoSQLAbbr('sql_while')<CR>
endif

if maparg('repeat?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> repeat? <C-R>=exsql#KyoSQLAbbr('sql_repeat')<CR>
endif

if maparg('loop?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> loop? <C-R>=exsql#KyoSQLAbbr('sql_loop')<CR>
endif

if maparg('utf8?', 'i', 1) == ""
    iabbr <silent> <unique> <buffer> utf8? <C-R>=exsql#KyoSQLAbbr('sql_utf8')<CR>
endif

if maparg('<leader>pd', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>pd :call exsql#showColumns('dbs', '', 1, 0)<CR>
endif

if maparg('<leader>pt', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>pt :call exsql#showColumns('tables', '', 1, 0)<CR>
endif

if maparg('<leader>pf', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>pf :call exsql#showColumns('columns', expand('<cword>'), 1, 0)<CR>
endif

if maparg('<leader>pf', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>pf :call exsql#showColumns('columns', '', 1, 1)<CR>
endif

if maparg('<F9>', 'x') == ""
    xnoremap <silent> <unique> <buffer> <F9> :call exsql#mysqlExport()<CR>
endif

" vim:ts=4:sw=4:sts=4 et fdm=marker:
