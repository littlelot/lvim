" local settings {{{
silent! setlocal buftype=nofile
silent! setlocal bufhidden=hide
silent! setlocal noswapfile
silent! setlocal nobuflisted

" silent! setlocal cursorline
silent! setlocal nowrap
" silent! setlocal statusline=
silent! setlocal number
" }}}

" Key Mappings Binding {{{
call exsql#bind_preview_mappings()
" }}}

" vim:ts=4:sw=4:sts=4 et fdm=marker:
