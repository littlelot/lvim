" default configuration {{{
" Result
if !exists('g:ex_sql_winsize')
  let g:ex_sql_winsize = 20
endif

if !exists('g:ex_sql_winsize_zoom')
  let g:ex_sql_winsize_zoom = 30
endif

if !exists('g:ex_sql_winpos')
  let g:ex_sql_winpos = 'bottom'
endif

if !exists('g:ex_sql_enable_help')
  let g:ex_sql_enable_help = 1
endif

" Preview
if !exists('g:ex_sql_preview_dbs_winsize')
  let g:ex_sql_preview_dbs_winsize = 30
endif

if !exists('g:ex_sql_preview_dbs_winsize_zoom')
  let g:ex_sql_preview_dbs_winsize_zoom = 50
endif

if !exists('g:ex_sql_preview_columns_winsize')
  let g:ex_sql_preview_columns_winsize = 80
endif

if !exists('g:ex_sql_preview_columns_winsize_zoom')
  let g:ex_sql_preview_columns_winsize_zoom = 100
endif

if !exists('g:ex_sql_preview_winpos')
  let g:ex_sql_preview_winpos = 'right'
endif

if !exists('g:ex_sql_preview_enable_help')
  let g:ex_sql_preview_enable_help = 1
endif
" }}}

" commands {{{
command! EXSqlOpen call exsql#open_window()
command! EXSqlClose call exsql#close_window()
command! EXSqlToggle call exsql#toggle_window()

command! EXSqlPreviewOpen call exsql#open_preview_window()
command! EXSqlPreviewClose call exsql#close_preview_window()
command! EXSqlPreviewToggle call exsql#toggle_preview_window()
" }}}

" default key mappings {{{
call exsql#register_hotkey( 2  , 1, '<leader><ESC>'   , ":call exsql#close_window()<CR>"                       , 'Close window.' )
call exsql#register_hotkey( 3  , 1, '<Space>'         , ":call exsql#toggle_zoom()<CR>"                        , 'Zoom in/out data window.' )

call exsql#register_preview_hotkey( 2  , 1, '<leader><ESC>'  , ":call exsql#close_preview_window()<CR>"              , 'Close window.' )
call exsql#register_preview_hotkey( 3  , 1, '<Space>'        , ":call exsql#toggle_preview_zoom()<CR>"               , 'Zoom in/out preview window.' )
call exsql#register_preview_hotkey( 4  , 1, '<Enter>'        , ":call exsql#showColumns('enter', '', 0, 0)<CR>"      , 'Preview window forwards.' )
call exsql#register_preview_hotkey( 5  , 1, '<C-\>'          , ":call exsql#showColumns('shift+enter', '', 0, 0)<CR>", 'Preview window backwards' )
" }}}

call ex#register_plugin( 'exsql', {} )
call ex#register_plugin( 'exsqlpreview', {} )

" vim:ts=4:sw=4:sts=4 et fdm=marker:
