if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

" syntax highlight
set syntax=sql
" syntax match ex_sql_help #^".*# contains=ex_sql_help_key
" syntax match ex_sql_help_key '^" \S\+:'hs=s+2,he=e-1 contained contains=ex_sql_help_comma
" syntax match ex_sql_help_comma ':' contained

" hi default link ex_sql_help Comment
" hi default link ex_sql_help_key Label
" hi default link ex_sql_help_comma Special

let b:current_syntax = "exsql"

" vim:ts=4:sw=4:sts=4 et fdm=marker:
