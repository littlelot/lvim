" default configuration {{{
if !exists('g:ex_mog_winsize')
    let g:ex_mog_winsize = 20
endif

if !exists('g:ex_mog_winsize_zoom')
    let g:ex_mog_winsize_zoom = 20
endif

" bottom or top
if !exists('g:ex_mog_winpos')
    let g:ex_mog_winpos = 'bottom'
endif

if !exists('g:ex_mog_enable_help')
    let g:ex_mog_enable_help = 1
endif

" }}}

" commands {{{
command! EXMogOpen call exmog#open_window()
command! EXMogClose call exmog#close_window()
command! EXMogToggle call exmog#toggle_window()
" }}}

" default key mappings {{{
call exmog#register_hotkey( 2  , 1, '<leader><ESC>'   , ":call exmog#close_window()<CR>"    , 'Close window.' )
" call exmog#register_hotkey( 3  , 1, '<Space>'         , ":call exmog#toggle_zoom()<CR>"     , 'Zoom in/out data window.' )
" }}}

call ex#register_plugin( 'json', {} )

" vim:ts=4:sw=4:sts=4 et fdm=marker:
