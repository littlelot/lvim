" mappings and iabbrs

if maparg('<leader>sq', 'n') == ""
    nnoremap <silent> <unique> <buffer> <leader>sq :call exmog#LittlelotMongoDBCmdView(0)<CR>
endif

if maparg('<leader>sq', 'x') == ""
    xnoremap <silent> <unique> <buffer> <leader>sq :call exmog#LittlelotMongoDBCmdView(1)<CR>
endif

" vim:ts=4:sw=4:sts=4 et fdm=marker:
