" default configuration {{{
if !exists('g:ex_java_winsize')
    let g:ex_java_winsize = 10
    " let g:ex_java_winsize = 20
endif

if !exists('g:ex_java_winsize_zoom')
    let g:ex_java_winsize_zoom = 10
endif

" bottom or top
if !exists('g:ex_java_winpos')
    let g:ex_java_winpos = 'bottom'
endif

if !exists('g:ex_java_enable_help')
    let g:ex_java_enable_help = 1
endif

if !exists('g:ex_mvn_cache_path')
    let g:ex_mvn_cache_path = '~/.mvnCache'
endif

if !exists('g:ex_java_search_path')
    let g:ex_java_search_path = '~/.__source'
endif

" }}}

" commands {{{
command! EXJavaOpen call exjava#open_window()
command! EXJavaClose call exjava#close_window()
command! EXJavaToggle call exjava#toggle_window()
" }}}

" default key mappings {{{
" call exjava#register_hotkey( 1  , 1, '<F1>'            , ":call exjava#toggle_help()<CR>"                        , 'Toggle help.' )
call exjava#register_hotkey( 2  , 1, '<leader><ESC>'   , ":call exjava#close_window()<CR>"                       , 'Close window.' )
call exjava#register_hotkey( 3  , 1, '<Space>'         , ":call exjava#toggle_zoom()<CR>"                        , 'Zoom in/out project window.' )
call exjava#register_hotkey( 4  , 1, '<CR>'            , ":call exjava#confirm_select()<CR>"                     , 'Insert java search result.' )
" }}}

call ex#register_plugin( 'exjava', {} )

" vim:ts=4:sw=4:sts=4 et fdm=marker:
