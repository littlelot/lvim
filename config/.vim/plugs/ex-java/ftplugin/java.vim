" exjava.vim -- Manage java imports
" Author: Ding Liu <18123715300@163.com>
" Requires: Vim-7.0 or higher
" Version: 1.0
" Licence: This program is free software; you can redistribute it and/or
"          modify it under the terms of the GNU General Public License.
"          See http://www.gnu.org/copyleft/gpl.txt
" Summary Of Features:
"   Insert import
"   Sort And Distinct imports
"   Reset Unused imports
" Usage:
"   Copy this file in your vim plugin folder
"   No classpath or configuration needed. This plugin use the regular vim search.
"   So you only need a good search index (through ctags or cscope for example)
" Commands:

if maparg('<leader>si', 'n') != ""
    nunmap <leader>si
endif

if maparg('<leader>ss', 'n') != ""
    nunmap <leader>ss
endif

nnoremap <silent> <unique> <buffer> <leader>si :call exjava#Vimporter_JavaLib(expand("<cword>"))<CR>
nnoremap <silent> <unique> <buffer> <leader>ss :call exjava#JavaSortImport()<CR>

iabbr <silent> <unique> <buffer> if if () {<CR>}<Esc>k$hhi<C-R>=exjava#RemoveSpaceOrTabOrEnter('\s')<CR>
iabbr <silent> <unique> <buffer> for for () {<CR>}<Esc>k$hhi<C-R>=exjava#RemoveSpaceOrTabOrEnter('\s')<CR>
iabbr <silent> <unique> <buffer> catch catch () {<CR>}<Esc>k$hhi<C-R>=exjava#RemoveSpaceOrTabOrEnter('\s')<CR>
iabbr <silent> <unique> <buffer> while while () {<CR>}<Esc>k$hhi<C-R>=exjava#RemoveSpaceOrTabOrEnter('\s')<CR>
iabbr <silent> <unique> <buffer> switch switch () {<CR>}<Esc>k$hhi<C-R>=exjava#RemoveSpaceOrTabOrEnter('\s')<CR>
iabbr <silent> <unique> <buffer> { {<CR>}<Esc>ko<C-R>=exjava#RemoveSpaceOrTabOrEnter('\r')<CR>
