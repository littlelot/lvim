if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

" syntax highlight
syntax match ex_ja_help #^".*# contains=ex_ja_help_key
syntax match ex_ja_help_key '^" \S\+:'hs=s+2,he=e-1 contained contains=ex_ja_help_comma
syntax match ex_ja_help_comma ':' contained
syntax match ex_ja_class_path /^\([0-9a-zA-Z]\+\.\)\+.*$/

hi default link ex_ja_help Comment
hi default link ex_ja_help_key Label
hi default link ex_ja_help_comma Special
hi default link ex_ja_class_path Directory

let b:current_syntax = "exjava"

" vim:ts=4:sw=4:sts=4 et fdm=marker:
