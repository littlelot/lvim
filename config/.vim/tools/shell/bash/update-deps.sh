#!/bin/bash

DEP_LIST="${VIMENTRYDIR}/deps.list"
DEP_NEW_LIST="${VIMENTRYDIR}/deps.new.list"
DEP_TAG_LIST="${VIMENTRYDIR}/deps.tag.list"
DEP_TMP_LIST="${VIMENTRYDIR}/deps.tmp.list"
DEP_CLASS_LIST="${VIMENTRYDIR}/deps.class.list"
DEP_CLASS_NEW_LIST="${VIMENTRYDIR}/deps.class.new.list"
DEP_SOURCE_LIST="${VIMENTRYDIR}/deps.source.list"
DEP_SOURCE_NEW_LIST="${VIMENTRYDIR}/deps.source.new.list"

# update deps
echo "Updating Deps..."
beginTime=`date +%s`
cd "${WORKDIR}"

# parse deps
deps=`mvn dependency:list -Dsort=true                                 \
    | grep -E '^\[INFO\]\s*\S+:\S+:jar:\S+:\S+\s*$'                   \
    | sed -r 's/^\[INFO\]\s*(\S+):(\S+):jar:(\S+):\S+\s*$/\1 \2 \3/g' \
    | sort -u`
endTime=`date +%s`
echo "  |- parse deps,cost $((${endTime}-${beginTime})) (s)"

# comparison new and old deps
if [ ! x = x"${deps}" ]; then
    beginTime="${endTime}"
    [ -f "${DEP_TAG_LIST}" ] && rm "${DEP_TAG_LIST}"
    [ -f "${DEP_TMP_LIST}" ] && rm "${DEP_TMP_LIST}"
    while read groupId artifactId version
    do
        depDir="${DEST}/${groupId//./\/}/${artifactId}/${version}"
        depJar="${depDir}/${artifactId}-${version}.jar"
        depSourceJar="${depDir}/${artifactId}-${version}-sources.jar"
        depMixJar="${depDir}/${artifactId}-${version}(sources).jar"
        tagDir="${depDir}/.__source"
        tagFile="${tagDir}/tags"

        flag="1"
        if [ -f "${DEP_LIST}" ]; then
            for oldDepJar in `cat "${DEP_LIST}"`
            do
                if [ x"${oldDepJar}" = x"${depJar}" ]; then
                    flag="2"
                    break
                fi
            done
        fi

        if [ x1 = x"${flag}" -a ! -f "${tagFile}" ]; then
            [ ! -d "${tagDir}" ] && mkdir -p "${tagDir}"
            cd "${tagDir}"
            if [ -f "${depSourceJar}" ]; then
                jar -xf "${depSourceJar}"
                [ "$?" -eq 0 ] && echo "  |- uncompress ${depSourceJar}" || echo "  |- uncompress ${depSourceJar} failed"
            elif [ -f "${depJar}" ]; then
                ${TOOLS}/decompiler/jd-cli "${depJar}" "${tagDir}" &> /dev/null
                [ "$?" -eq 0 ] && echo "  |- decompile ${depJar}" || echo "  |- decompile ${depJar} failed"
            else
                echo "  |- ${depMixJar} not exists"
            fi

            if [ -f "${depSourceJar}" -o -f "${depJar}" ]; then
                find "${tagDir}" -type f -name '*.java' | ctags --fields=+iazSKlmn --extra=+qf -L -
                [ "$?" -eq 0 ] && echo "  |- generate ${tagFile}" || echo "  |- generate ${tagFile} failed"
            fi
        fi

        if [ x1 = x"${flag}" -a -f "${depJar}" ]; then
            jar -tf "${depJar}" | grep -E '^((\w+\/)+\w+)(\$\S+)?\.class$' | sed -r 's/^(.*)\.class$/\1/g' > "${DEP_TMP_LIST}"
            cat "${DEP_TMP_LIST}" | sed -r 's/\/|\$/./g' >> "${DEP_CLASS_LIST}"
            cat "${DEP_TMP_LIST}" | sed -r 's/^((\w+\/)+\w+)(\$\S+)?$/'"${tagDir//\//\\\/}"':\1.java/g' >> "${DEP_SOURCE_LIST}"
        fi

        echo "${depJar}" >> "${DEP_NEW_LIST}"
        echo "${tagFile}" >> "${DEP_TAG_LIST}"
    done <<< `echo -e "${deps}"`
    [ -f "${DEP_TMP_LIST}" ] && rm "${DEP_TMP_LIST}"
    endTime=`date +%s`
    echo "  |- update deps,cost $((${endTime}-${beginTime})) (s)"
else
    echo "  |- parse deps failed"
    exit 1
fi

# sort deps.class.list
if [ -f "${DEP_CLASS_LIST}" ]; then
    beginTime="${endTime}"
    cat "${DEP_CLASS_LIST}" | sort -u > "${DEP_CLASS_NEW_LIST}"
    mv -f "${DEP_CLASS_NEW_LIST}" "${DEP_CLASS_LIST}"
    endTime=`date +%s`
    echo "  |- sort ${DEP_CLASS_LIST},cost $((${endTime}-${beginTime})) (s)"
fi

# sort deps.source.list
if [ -f "${DEP_SOURCE_LIST}" ]; then
    beginTime="${endTime}"
    cat "${DEP_SOURCE_LIST}" | sort -u > "${DEP_SOURCE_NEW_LIST}"
    mv -f "${DEP_SOURCE_NEW_LIST}" "${DEP_SOURCE_LIST}"
    endTime=`date +%s`
    echo "  |- sort ${DEP_SOURCE_LIST},cost $((${endTime}-${beginTime})) (s)"
fi

# replace old deps.list
beginTime="${endTime}"
mv -f "${DEP_NEW_LIST}" "${DEP_LIST}"
endTime=`date +%s`
echo "  |- move ${DEP_NEW_LIST} to ${DEP_LIST},cost $((${endTime}-${beginTime})) (s)"

# complete
echo "  |- done!"
