#!/bin/bash

JRE_CLASS_LIST="${DEST}/jre.class.list"
JRE_CLASS_NEW_LIST="${DEST}/jre.class.new.list"

# update jre
echo "Updating Jre..."
beginTime=`date +%s`

# uncompress jre source
[ ! -d "${DEST}" ] && mkdir -p "${DEST}"
cd "${DEST}"

unzip -o "${JAVA_HOME}/src.zip" &> /dev/null
unzip -o "${JAVA_HOME}/javafx-src.zip" &> /dev/null
${TOOLS}/decompiler/jd-cli "${JAVA_HOME}/jre/lib/jce.jar" "${DEST}" &> /dev/null
endTime=`date +%s`
echo "  |- uncompress jre source,cost $((${endTime}-${beginTime})) (s)"

# generate tags
beginTime="${endTime}"
[ -f "${DEST}/tags" ] && rm "${DEST}/tags" -f
find "${DEST}" -type f -name '*.java' | ctags --fields=+iazSKlmn --extra=+qf -L -
endTime=`date +%s`
echo "  |- generate ${DEST}/tags,cost $((${endTime}-${beginTime})) (s)"

# append classpath
beginTime="${endTime}"
[ -f "${JRE_CLASS_NEW_LIST}" ] && rm "${JRE_CLASS_NEW_LIST}" -f
for jarFile in `find "${JAVA_HOME}" -type f -name '*.jar'`
do
    jar -tf "${jarFile}" | grep -E '^((\w+\/)+\w+)(\$\S+)?\.class$'    \
        | sed -r 's/^(.*)\.class$/\1/g' | sed -r 's/\/|\$/./g' >> "${JRE_CLASS_NEW_LIST}"
done
endTime=`date +%s`
echo "  |- append classpath to ${JRE_CLASS_NEW_LIST},cost $((${endTime}-${beginTime})) (s)"

# sort jre.class.new.list
beginTime="${endTime}"
cat "${JRE_CLASS_NEW_LIST}" | sort -u > "${JRE_CLASS_LIST}"
[ -f "${JRE_CLASS_NEW_LIST}" ] && rm "${JRE_CLASS_NEW_LIST}" -f
endTime=`date +%s`
echo "  |- sort ${JRE_CLASS_NEW_LIST},cost $((${endTime}-${beginTime})) (s)"

# complete
echo "  |- done!"
