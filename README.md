# Littlelot Vim
## Description
> #### littlelot vim project <u>安装脚本暂只支持debian包系</u>
> #### 实现java导包排序,运行,mysql分屏展示插件
> #### 增加常用实用插件,如exvim,vim-plug,easy-motion等
## Install
``` shell
git clone https://gitee.com/littlelot/lvim.git ~/.lvim
cd ~/.lvim
./linux_install.sh
```
## Plugin
> #### .vimrc 常用映射
> &emsp;&emsp;`[n] ;nh`&emsp;&emsp;&emsp;&emsp;关闭搜索高亮<br/>
> &emsp;&emsp;`[n] ;pa`&emsp;&emsp;&emsp;&emsp;开关粘贴模式\(保留粘贴原文格式\)<br/>
> &emsp;&emsp;`[n] ;nu`&emsp;&emsp;&emsp;&emsp;开关行号显示<br/>
> &emsp;&emsp;`[n] <Space>`&emsp;&emsp;开关折叠<br/>
> &emsp;&emsp;`[n] <C-H>`&emsp;&emsp;&emsp;移动光标到左边的窗口<br/>
> &emsp;&emsp;`[n] <C-J>`&emsp;&emsp;&emsp;移动光标到上边的窗口<br/>
> &emsp;&emsp;`[n] <C-K>`&emsp;&emsp;&emsp;移动光标到下边的窗口<br/>
> &emsp;&emsp;`[n] <C-L>`&emsp;&emsp;&emsp;移动光标到右边的窗口<br/>
> &emsp;&emsp;`[n] ;M`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;清除行尾 ^M 符号<br/>
> &emsp;&emsp;`[nx] "y`&emsp;&emsp;&emsp;&emsp;vim 中跨终端复制<br/>
> &emsp;&emsp;`[n] "p`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;vim 中跨终端粘贴<br/>
> &emsp;&emsp;`[n] ;y`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;复制到系统剪贴板,clipboard 特性开启时有效<br/>
> &emsp;&emsp;`[n] ;p`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;粘贴到系统剪贴板,clipboard 特性开启时有效<br/>
> &emsp;&emsp;`[n] ;ev`&emsp;&emsp;&emsp;&emsp;编辑 .vimrc 文件<br/>
> &emsp;&emsp;`[n] ;sv`&emsp;&emsp;&emsp;&emsp;重载 .vimrc 文件<br/>
> &emsp;&emsp;`[n] ;sp`&emsp;&emsp;&emsp;&emsp;java 打印语句补全<br/>
> &emsp;&emsp;`[n] ;sl`&emsp;&emsp;&emsp;&emsp;logger 打印语句补全<br/>
> &emsp;&emsp;`[nx] \`&nbsp;&emsp;&emsp;&emsp;&emsp;重定义 ';' 默认映射

> #### ex-vimentry
> &emsp;&emsp;`vim .exvim`&nbsp;&emsp;&emsp;&emsp;打开 exvim 项目工程入口文件,初始化项目设置<br/>
> &emsp;&emsp;`folder_filter_mode = exclude`&emsp;&emsp;设置目录过滤模式<br/>
> &emsp;&emsp;`folder_filter += target`&emsp;&emsp;设置相应过滤模式下的目录<br/>
> &emsp;&emsp;`build_opt = 'spring-boot:run'`&emsp;&emsp;设置外部编译运行命令<br/>
> &emsp;&emsp;`extra_tags = /home/littlelot/.__source/tags`&emsp;&emsp;设置 jdk 及第三方 jar 包源码 ctags 文件

> #### ex-project
> &emsp;&emsp;`[n] ;E`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;打开或关闭文件浏览器窗口<br/>
> &emsp;&emsp;`[n] ;R`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;重新生成项目目录树并刷新文件浏览器窗口<br/>
> &emsp;&emsp;`[n] ;r`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;重新生成当前所在目录的目录树并刷新当前目录<br/>
> &emsp;&emsp;`[n] O`&emsp;&emsp;&emsp;&emsp;&emsp;在当前目录下新建目录<br/>
> &emsp;&emsp;`[n] o`&emsp;&emsp;&emsp;&emsp;&emsp;在当前目录下新建文件<br/>
> &emsp;&emsp;`[n] ;sc`&emsp;&emsp;&emsp;&emsp;定位当前编辑文件在文件浏览器窗口的对应位置<br/>
> &emsp;&emsp;`[n] <CR>`&nbsp;&nbsp;&emsp;&emsp;&emsp;折叠目录或者打开相应的文件<br/>
> &emsp;&emsp;`[n] <Space>`&emsp;&emsp;收缩文件浏览器窗口<br/>
> &emsp;&emsp;`[n] <F1>`&nbsp;&nbsp;&emsp;&emsp;&emsp;显示文件浏览器窗口帮助信息

> #### ex-tagbar
> &emsp;&emsp;`[n] ;L`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;打开或关闭 tagbar 标签窗口<br/>
> &emsp;&emsp;`[n] v`&emsp;&emsp;&emsp;&emsp;&emsp;隐藏非公有的标签<br/>
> &emsp;&emsp;`[n] o`&emsp;&emsp;&emsp;&emsp;&emsp;打开或者关闭一个折叠<br/>
> &emsp;&emsp;`[n] *`&emsp;&emsp;&emsp;&emsp;&emsp;打开所有折叠<br/>
> &emsp;&emsp;`[n] =`&emsp;&emsp;&emsp;&emsp;&emsp;关闭所有折叠<br/>
> &emsp;&emsp;`[n] +`&emsp;&emsp;&emsp;&emsp;&emsp;打开一个折叠<br/>
> &emsp;&emsp;`[n] -`&emsp;&emsp;&emsp;&emsp;&emsp;关闭一个折叠<br/>
> &emsp;&emsp;`[n] <CR>`&nbsp;&nbsp;&emsp;&emsp;&emsp;跳转到标签所在位置<br/>
> &emsp;&emsp;`[n] <Space>`&emsp;&emsp;收缩 tagbar 窗口<br/>
> &emsp;&emsp;`[n] ;<Esc>`&nbsp;&emsp;&emsp;关闭 tagbar 窗口<br/>
> &emsp;&emsp;`[n] <F1>`&nbsp;&emsp;&emsp;&emsp;显示 tagbar 窗口帮助信息

> #### ex-qfix
> &emsp;&emsp;`[n] ;jr`&emsp;&emsp;&emsp;&emsp;启动或者重新启动一个 java-job<br/>
> &emsp;&emsp;`[n] ;V`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;打开或关闭 quick-fix 窗口<br/>
> &emsp;&emsp;`[n] <C-C>`&emsp;&emsp;&emsp;停止正在运行中的 java-job<br/>
> &emsp;&emsp;`[n] <CR>`&nbsp;&nbsp;&emsp;&emsp;&emsp;跳转到对应类的源文件中的对应位置<br/>
> &emsp;&emsp;`[n] ;<Esc>`&nbsp;&emsp;&emsp;关闭 quick-fix 窗口

> #### ex-java
> &emsp;&emsp;`[n] ;si`&emsp;&emsp;&emsp;&emsp;查找并导入当前光标下的类,如果有多个则打开 java 搜索结果展示窗口<br/>
> &emsp;&emsp;`[n] ;ss`&emsp;&emsp;&emsp;&emsp;排序当前文件中的所有 import 语句<br/>
> &emsp;&emsp;`[n] ;J`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;打开或关闭 java 搜索结果展示窗口<br/>
> &emsp;&emsp;`[n] <CR>`&nbsp;&nbsp;&emsp;&emsp;&emsp;插入当前行的 import 语句到之前编辑文件的特定位置<br/>
> &emsp;&emsp;`[n] ;<Esc>`&nbsp;&emsp;&emsp;关闭 java 搜索结果展示窗口

> #### ex-sql
> &emsp;&emsp;`[nx] ;sq`&nbsp;&nbsp;&emsp;&emsp;&emsp;把选中行当做 sql 语句去执行并打开 mysql 执行结果展示窗口<br/>
> &emsp;&emsp;`[n] ;st`&emsp;&emsp;&emsp;&emsp;生成执行时间<br/>
> &emsp;&emsp;`[n] ;sg`&emsp;&emsp;&emsp;&emsp;生成私有 mysql 配置<br/>
> &emsp;&emsp;`[n] ;N`&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;打开或关闭 mysql 执行结果展示窗口<br/>
> &emsp;&emsp;`[i] <F3>`&nbsp;&nbsp;&emsp;&emsp;&emsp;补全表名<br/>
> &emsp;&emsp;`[i] <F5>`&nbsp;&nbsp;&emsp;&emsp;&emsp;补全数据库名<br/>
> &emsp;&emsp;`[n] ;<Esc>`&nbsp;&emsp;&emsp;关闭 mysql 执行结果展示窗口

> #### ex-mog
> &emsp;&emsp;`[nx] ;sq`&nbsp;&nbsp;&emsp;&emsp;&emsp;把选中行当做 js 语句去执行并打开 mongodb 执行结果展示窗口<br/>
> &emsp;&emsp;`[n] ;<Esc>`&nbsp;&emsp;&emsp;关闭 mongodb 执行结果展示窗口

> #### ex-tags
> &emsp;&emsp;`[n] <C-]>`&emsp;&emsp;ctags 标签跳转列表<br/>
> &emsp;&emsp;`[n] <C-\>`&emsp;&emsp;ctags 标签跳转列表\(窗口预览模式\)

> #### ex-cscope
> &emsp;&emsp;`[n] ;;E`&emsp;&emsp;查找本egrep模式\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;e`&emsp;&emsp;查找本egrep模式<br/>
> &emsp;&emsp;`[n] ;;T`&emsp;&emsp;查找本字符串\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;t`&emsp;&emsp;查找本字符串<br/>
> &emsp;&emsp;`[n] ;;C`&emsp;&emsp;查找调用本函数的函数\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;c`&emsp;&emsp;查找调用本函数的函数<br/>
> &emsp;&emsp;`[n] ;;D`&emsp;&emsp;查找本函数调用的函数\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;d`&emsp;&emsp;查找本函数调用的函数<br/>
> &emsp;&emsp;`[n] ;;G`&emsp;&emsp;查找本定义\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;g`&emsp;&emsp;查找本定义<br/>
> &emsp;&emsp;`[n] ;;S`&emsp;&emsp;查找本C符号\(窗口预览模式\)<br/>
> &emsp;&emsp;`[n] ;;s`&emsp;&emsp;查找本C符号

> #### vim-better-whitespace
> &emsp;&emsp;`[n] ;ws`&nbsp;&nbsp;&nbsp;&emsp;&emsp;去除行尾多余空格,具体用法参见 [vim-better-whitespace][1]

> #### nerdcommenter
> &emsp;&emsp;`[nx] ;cc`&emsp;&emsp;单行注释\(可批量,所有类型文件通用\),具体用法参见 [nerdcommenter][1]<br/>
> &emsp;&emsp;`[nx] ;ci`&emsp;&emsp;反转注释\(可反转单行注释和性感的注释,所有类型文件通用\)<br/>
> &emsp;&emsp;`[nx] ;cs`&emsp;&emsp;性感的注释\(java 文件为文档注释,适用于 c,c++,c#,java 等文件类型\)<br/>
> &emsp;&emsp;`[nx] ;cu`&emsp;&emsp;取消注释\(可取消单行注释和性感的注释,所有类型文件通用\)<br/>
> &emsp;&emsp;`[nx] ;cm`&emsp;&emsp;多行注释\(java 文件专用,形如 /\* aaa \*/\)<br/>
> &emsp;&emsp;`[nx] ;cn`&emsp;&emsp;取消多行注释\(只适用于取消多行注释,取消嵌套注释功能暂未实现,java 文件专用\)

> #### ctrlp.vim
> &emsp;&emsp;`[n] <C-P>`&emsp;&emsp;模糊搜索文件列表并打开窗口展示,具体用法参见 [ctrlp.vim][2]<br/>
> &emsp;&emsp;`[n] ;B`&nbsp;&nbsp;&emsp;&emsp;&emsp;当前缓冲区文件列表,具体用法参见 [ctrlp.vim][2]<br/>
> &emsp;&emsp;`[n] ;F`&nbsp;&nbsp;&emsp;&emsp;&emsp;最近打开文件列表历史,具体用法参见 [ctrlp.vim][2]<br/>
> &emsp;&emsp;`[w] <F5>`&nbsp;&nbsp;&emsp;&emsp;清除缓存,刷新当前模式下模糊搜索文件列表<br/>
> &emsp;&emsp;`[w] <C-F>`&emsp;&emsp;在 file,buffer,mru 三种模式间来回切换<br/>
> &emsp;&emsp;`[w] <C-B>`&emsp;&emsp;在 file,buffer,mru 三种模式间来回切换<br/>
> &emsp;&emsp;`[w] <C-D>`&emsp;&emsp;切换文件搜索方式为文件名搜索或全路径搜索<br/>
> &emsp;&emsp;`[w] <C-R>`&emsp;&emsp;切换文件搜索方式为正则模式或非正则模式<br/>
> &emsp;&emsp;`[w] <C-J>`&emsp;&emsp;ctrlp 窗口中上下移动并选中光标行<br/>
> &emsp;&emsp;`[w] <C-K>`&emsp;&emsp;ctrlp 窗口中上下移动并选中光标行<br/>
> &emsp;&emsp;`[w] <C-T>`&emsp;&emsp;预览选中行,并在一个新窗口中打开 buffer<br/>
> &emsp;&emsp;`[w] <C-V>`&emsp;&emsp;预览选中行,并在一个新窗口中打开 buffer<br/>
> &emsp;&emsp;`[w] <C-X>`&emsp;&emsp;预览选中行,并在一个新窗口中打开 buffer<br/>
> &emsp;&emsp;`[w] <C-N>`&emsp;&emsp;跳转上一个/下一个模糊搜索历史,非常有用<br/>
> &emsp;&emsp;`[w] <C-P>`&emsp;&emsp;跳转上一个/下一个模糊搜索历史,非常有用<br/>
> &emsp;&emsp;`[w] <C-Y>`&emsp;&emsp;创建新文件和它的父级目录,打开一个新的 buffer,:w 之后才会存盘写入文件<br/>
> &emsp;&emsp;`[w] <C-Z>`&emsp;&emsp;标记多个文件 `<C-O>` 同时打开已标记的多个文件

> #### vim-surround
> &emsp;&emsp;`[x] s`&nbsp;&nbsp;&emsp;&emsp;给选中文本以 " ' \( \[ \{ < \` 等特殊符号包围<br/>
> &emsp;&emsp;`[n] cs`&emsp;&emsp;改变包围文本的特殊符号,具体用法参见 [vim-surround][3]<br/>
> &emsp;&emsp;`[n] ds`&emsp;&emsp;删除包围文本的特殊符号,具体用法参见 [vim-surround][3]<br/>
> &emsp;&emsp;`[n] ys`&emsp;&emsp;给选中的指定的文本对象以 " ' \( \[ \{ < \` 等特殊符号包围

> #### vim-easy-align
> &emsp;&emsp;`[n] ga`&emsp;&emsp;普通模式下对齐文本前置键,具体用法参见 [vim-easy-align][4]<br/>
> &emsp;&emsp;`[x] <Space>`&emsp;&emsp;可视模式下对齐文本前置键,具体用法参见 [vim-easy-align][4]

> #### vim-easymotion
> &emsp;&emsp;`[nx] ;;h`&emsp;&emsp;按单词词头显示跳转字符<br/>
> &emsp;&emsp;`[nx] ;;j`&emsp;&emsp;按光标所在行显示跳转字符<br/>
> &emsp;&emsp;`[nx] ;;k`&emsp;&emsp;按光标所在列显示跳转字符<br/>
> &emsp;&emsp;`[nx] ;;l`&emsp;&emsp;按单词词尾显示跳转字符<br/>
> &emsp;&emsp;`[nx] ;h`&nbsp;&nbsp;&emsp;&emsp;按 W 显示跳转字符,具体用法参见 [vim-easymotion][5]<br/>
> &emsp;&emsp;`[nx] ;l`&nbsp;&nbsp;&emsp;&emsp;按 E 显示跳转字符,具体用法参见 [vim-easymotion][5]<br/>
> &emsp;&emsp;`[nx] ;f`&nbsp;&nbsp;&emsp;&emsp;按 f 查找并显示跳转字符,具体用法参见 [vim-easymotion][5]

> #### emmet-vim
> &emsp;&emsp;`[i] <C-Y>,`&emsp;&emsp;html 辅助编写,具体用法参见 [emmet-vim][6]

> #### vim-autoformat
> &emsp;&emsp;`[nx] <F2>`&emsp;&emsp;按文件类型格式化选中文本或默认格式化整个文件,具体用法参见 [vim-autoformat][7]
## Coding Tech
* 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
* 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
* 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
* [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
* 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
* 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

[0]: https://github.com/vim-colors-solarized
[1]: https://github.com/scrooloose/nerdcommenter
[2]: https://github.com/kien/ctrlp.vim
[3]: https://github.com/tpope/vim-surround
[4]: https://github.com/junegunn/vim-easy-align
[5]: https://github.com/Lokaltog/vim-easymotion
[6]: https://github.com/mattn/emmet-vim
[7]: https://github.com/Chiel92/vim-autoformat
