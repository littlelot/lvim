#!/bin/bash

install_dir=$(pwd)/config
dependency_cmd="exuberant-ctags cscope flameshot astyle tidy sqlformat xclip"

err_exit() {
    echo $1
    exit $2
}

install_package() {
    dpkg -s "$1" &> /dev/null
    if [ "$?" -eq 0 ]; then
        echo "$1"" 已经安装,无需再次安装"
    else
        sudo apt-get install "$1" -y &> /dev/null
        [ "$?" -eq 0 ] && echo "$1"" 安装成功!" || err_exit "$1"" 安装失败,请检查镜像源是否正确配置!"
    fi
}

dependency_parse() {
    for outer_cmd in $dependency_cmd
    do
        install_package $outer_cmd
    done
}

create_link() {
    # 删除旧的配置或者旧的软链接文件
    rm ~/.vim* -rf &> /dev/null

    ln -s $install_dir/.vim ~/.vim
    ln -s $install_dir/.vimrc ~/.vimrc

    echo "lvim 安装成功!"
}

# dependency_parse
create_link
